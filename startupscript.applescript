(*
		EN: Versionhistory:
		1.5.1	correcting some spelling issues
		1.5		deleting some folders
		1.2		checks if the volume needed for iTunes is already mounted
		1.0		mounts the network drive and starts itunes

*)
(*

	What does this script do?
	1. It tell the application Finder to do every check within his responibility.
	2. Tries to open the folder music as a volume if it's there
		2.1 If that fails the volume is mounted as smb volume. Correct this if you want afp (access via info-panel on Finder)
		2.2 if this is working a ssh command is executed
	3. to get speed and a better overview the deletion of the folder test is done via a ssh command. This is because
		AppleScipt doesn't delete a not empty folder. To do this via AppleScript, the script would run for a few minutes or hours. 

	4. After all this iTunes is launched.
*)

tell application "Finder"
	try
		open folder "/Volumes/music"
	on error
		mount volume "smb://delta.local/music"
	end try
	try
		do shell script "rm -rf /Volumes/music/test"
	end try
end tell
tell application "iTunes"
	launch
end tell